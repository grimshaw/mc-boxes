#ifndef Miniclip_AssetManager_h__
#define Miniclip_AssetManager_h__

#include "Utilities.h"
#include "Texture.h"

#include <string>
#include <map>
#include <memory>
#include <assert.h>

class Texture;
class GameLoop;
struct SDL_Renderer;

/**
	\class AssetManager

	For convenience, the asset manager is accessible as a "singleton"
	even though its properly encapsulated in the GameLoop class.

	The only alive AssetManager in the prototype is running within the
	only GameLoop instance. The methods in this class are static and use the
	manually set "gInstance" static member to operate.
*/
class AssetManager
{
private:
	
	friend class GameLoop;
	static AssetManager* gInstance;

	// Now the non-static members

	typedef std::unique_ptr<Texture> TexturePtr;

	std::map<std::string, TexturePtr> m_textures;

	/// A global reference to the renderer is kept to allow for texture loading
	SDL_Renderer* m_globalRenderer;
	
public:

	/// Initialize the non static members
	AssetManager();

	/// Creates a new texture in the manager. Returns nullptr if it couldn't be created
	static Texture* CreateTexture(const std::string& name);

	/// Get the texture resource by its name (file path)
	static Texture* GetTexture(const std::string& name);
};

#endif // Miniclip_AssetManager_h__