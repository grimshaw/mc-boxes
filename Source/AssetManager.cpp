#include "AssetManager.h"

AssetManager* AssetManager::gInstance = nullptr;


AssetManager::AssetManager()
: m_globalRenderer(nullptr)
{
}

Texture* AssetManager::CreateTexture(const std::string& name)
{
	assert(gInstance != nullptr);
	assert(gInstance->m_globalRenderer != nullptr);

	auto it = gInstance->m_textures.find(name);
	if (it != gInstance->m_textures.end())
	{
		printf("You tried to create a texture that already existed\n");
		return it->second.get();
	}

	auto obj = std::make_unique<Texture>();
	if (obj->loadFromFile(name, gInstance->m_globalRenderer))
	{
		printf("> Texture loaded '%s'\n", name.c_str());
		gInstance->m_textures[name] = std::move(obj);
		return gInstance->m_textures[name].get();
	}
	else
	{
		return nullptr;
	}
}

Texture* AssetManager::GetTexture(const std::string& name)
{
	assert(gInstance != nullptr);

	auto it = gInstance->m_textures.find(name);
	if (it != gInstance->m_textures.end())
	{
		return it->second.get();
	}
	else
		return nullptr;
}