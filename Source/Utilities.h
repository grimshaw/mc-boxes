#ifndef Miniclip_Utilities_h__
#define Miniclip_Utilities_h__

#include <stdint.h>
#include <random>
#include <ctime>
#include <functional>

extern std::mt19937 rng;

int RandomBoxColor();

typedef std::function<void(float)> AnimatorFunc;

class Animator
{
private:
	float        m_elapsed;   //< Tracks the time that passed since the animation start (ends when equal to delay + duration)
	float        m_duration;  //< The duration specifies how fast the animation will play
	float        m_delay;     //< The delay allows pushed animations to start a bit later
	AnimatorFunc m_effector;  //< Called continuously with elapsed time, to actually update the object
};

class Color
{
public:
	uint8_t r;
	uint8_t g;
	uint8_t b;
	uint8_t a;

	Color()
		: r(255)
		, g(255)
		, b(255)
		, a(255)
	{
	}

	Color(uint8_t nr, uint8_t ng, uint8_t nb, uint8_t na = 255)
		: r(nr)
		, g(ng)
		, b(nb)
		, a(na)
	{
	}

};

class Vector2
{
public:
	float x;
	float y;
	
	/// Construct an uninitialized vector
	Vector2();
	
	/// Constructs the vector from values
	Vector2(float xvalue, float yvalue);
	
	/// Convenient to multiply the vector with a scalar
	Vector2 operator*(float scalar);
	
	/// Convenient to multiply the vector with another vector
	Vector2 operator*(const Vector2& v);
};

#endif // Miniclip_Utilities_h__