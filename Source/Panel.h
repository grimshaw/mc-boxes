#ifndef Miniclip_Panel_h__
#define Miniclip_Panel_h__

#include "Widget.h"
#include "Utilities.h"

#include <string>
#include <vector>
#include <memory>

class Panel : public Widget 
{
private:

	typedef std::unique_ptr<Widget> WidgetPtr;

	/// The child items this panel has
	std::vector<WidgetPtr> m_items;

public:

	/// Instances and adds the widget as a child
	template<class T>
	void addItem(Vector2 position, Vector2 size);
};

template<class T>
void Panel::addItem(Vector2 position, Vector2 size)
{
	auto obj = std::make_unique<T>();
	obj->setSize(size);
	obj->setPosition(position);
	m_items.push_back(std::move(obj));
}

#endif // Miniclip_Panel_h__