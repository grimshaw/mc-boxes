#include "Texture.h"
#include "SDL_image.h"

Texture::Texture()
: m_bitmap(nullptr)
, m_texture(nullptr)
{

}

Texture::~Texture()
{
	if (m_bitmap)
	{
		SDL_FreeSurface(m_bitmap);
	}

	if (m_texture)
	{
		SDL_DestroyTexture(m_texture);
	}
}

SDL_Texture* Texture::getTextureId()
{
	return m_texture;
}

bool Texture::loadFromFile(const std::string& assetPath, SDL_Renderer* renderer)
{
	m_bitmap = IMG_Load(assetPath.c_str());

	if (!m_bitmap)
	{
		printf("Failed to load image '%s'\n", assetPath.c_str());
		return false;
	}

	m_texture = SDL_CreateTextureFromSurface(renderer, m_bitmap);

	if (!m_texture)
	{
		printf("Failed to prepare texture for '%s'\n", assetPath.c_str());
		return false;
	}

	return true;
}