#include "GameLoop.h"

GameLoop::GameLoop()
: m_accumulatedTime(0.f)
, m_closing(false)
, m_previousTime(0)
{	
	AssetManager::gInstance = &m_assetManager;

	m_board.init(16,10);
	m_board.generate(10);
}
	
void GameLoop::init()
{
	m_assetManager.m_globalRenderer = m_renderer;

	AssetManager::CreateTexture("Textures/full-background.png");
}

int GameLoop::exec()
{
	SDL_Init(SDL_INIT_EVERYTHING);
	IMG_Init(IMG_INIT_PNG);

	m_window = SDL_CreateWindow("Miniclip Challenge - Artur Moreira", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1200, 700, SDL_WINDOW_SHOWN | SDL_WINDOWPOS_CENTERED);
	m_renderer = SDL_CreateRenderer(m_window, -1, SDL_RENDERER_ACCELERATED);

	assert(m_window);
	assert(m_renderer);

	printf("> Window initialized\n");

	// Load the assets
	init();

	while(!m_closing)
		tick();
	
	IMG_Quit();
	SDL_Quit();
	return 0;
}	

void GameLoop::tick()
{
	SDL_Event e;
	Uint32 currentTime = SDL_GetTicks();
	float deltaTime = static_cast<float>(currentTime - m_previousTime) / 1000.f;
	m_previousTime = currentTime;

	m_accumulatedTime += deltaTime;

	SDL_Rect viewportRect;
	viewportRect.x = 0;
	viewportRect.y = 600;
	viewportRect.w = 800;
	viewportRect.h = -600;

	
	// Fixed update time step at 60hz
	while(m_accumulatedTime >= 1.f / 60.f)
	{
		// Handle input
		while (SDL_PollEvent(&e))
		{
			if (e.type == SDL_QUIT)
			{
				m_closing = true;
			}
			else if (e.type == SDL_MOUSEBUTTONDOWN)
			{
				m_board.handleClick(Vector2(e.button.x, e.button.y));
			}
			else if (e.type == SDL_MOUSEMOTION)
			{
				m_board.handleMouseMove(Vector2(e.motion.x, e.motion.y));
			}
		}
		
		// Update animations and timers
		m_board.tick(1.f / 60.f);
		
		// Render the frame
		//SDL_RenderSetViewport(m_renderer, &viewportRect);
		SDL_SetRenderDrawColor(m_renderer, 0, 0, 0, 255);
		SDL_RenderClear(m_renderer);

		m_board.render(m_renderer);
	
		SDL_RenderPresent(m_renderer);

		m_accumulatedTime -= 1.f / 60.f;
	}
}
	