#ifndef Miniclip_Widget_h__
#define Miniclip_Widget_h__

#include "Utilities.h"
#include <string>

class Widget
{
private:
	Vector2     m_position;
	Vector2     m_size;

public:

	void setPosition(Vector2 position);

	void setSize(Vector2 size);
};

#endif // Miniclip_Widget_h__