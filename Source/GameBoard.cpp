#include "GameBoard.h"
#include "AssetManager.h"

std::vector<BoxRef> selection;

std::map<uint16_t, Color> colorTable
{
	{ 0, Color(100, 50, 80) },
	{ 1, Color(30, 110, 190) },
	{ 2, Color(160, 121, 220) },
	{ 3, Color(150, 156, 255) },
	{ 4, Color(20, 16, 180) },
	{ 5, Color(30, 90, 90) },
	{ 6, Color(255, 50, 80) },
	{ 7, Color(230, 80, 120) },
	{ 8, Color(80, 220, 190) },
	{ 9, Color(180, 10, 220) },
	{ 10, Color(190, 80, 10) },
	{ 11, Color(210, 190, 40) },
};

const float kTimeForPushes = 15.f;

GameBoard::GameBoard()
: m_boxSize(40)
, m_position(100, 0)
, m_timeLeftForPush(kTimeForPushes)
{
	
}
	
void GameBoard::init(int w, int h)
{
	m_boxes.resize(w * h);
	m_width = w;
	m_height = h;
	
	for(auto& box : m_boxes)
	{
		box.colorIndex = -1;
	}
}

void GameBoard::generate(int columnCount)
{
	for (std::size_t i = 0; i < m_width; ++i)
	{
		for (std::size_t j = 0; j < m_height; ++j)
		{
			if (i > (m_width - columnCount))
			{
				getBox(i, j).colorIndex = RandomBoxColor();
			}
			else
			{
				getBox(i, j).colorIndex = -1;
			}
		}
	}
}

void GameBoard::tick(float delta)
{
	m_timeLeftForPush -= delta;
	if(m_timeLeftForPush <= 0.f)
	{
		if(pushColumn())
		{
			// Game is over
		}
		else
			m_timeLeftForPush = kTimeForPushes;
	}
}
	
Box& GameBoard::getBox(int hindex, int vindex)
{
	return m_boxes[vindex * m_width + hindex];
}	

bool GameBoard::isSlotEmpty(int hindex, int vindex)
{
	return m_boxes[vindex * m_width + hindex].colorIndex == -1;
}
	
bool GameBoard::pushColumn()
{
	bool gameIsOver = false;
	
	for(std::size_t i = 0; i < m_width; ++i)
	{
		for(std::size_t j = 0; j < m_height; ++j)
		{
			// On the first column, any present box will fall
			if(i == 0 && !isSlotEmpty(i,j))
				gameIsOver = true;
			else if(i > 0 && !isSlotEmpty(i,j))
			{
				getBox(i - 1, j) = getBox(i, j);
			}
		}
	}

	// Generate the new column data
	for (std::size_t i = 0; i < m_height; ++i)
	{
		getBox(m_width - 1, i).colorIndex = RandomBoxColor();
	}
	
	return gameIsOver;
}
	
void GameBoard::render(SDL_Renderer* renderer)
{
	// Render the background
	SDL_Rect srcRect, dstRect;
	srcRect.x = srcRect.y = 0;
	srcRect.w = 3072;
	srcRect.h = 1536;

	dstRect.x = dstRect.y = 0;
	dstRect.w = 1200;
	dstRect.h = 700;
	SDL_RenderCopy(renderer, AssetManager::GetTexture("Textures/full-background.png")->getTextureId(), &srcRect, &dstRect);

	for (std::size_t i = 0; i < m_boxes.size(); ++i)
	{
		Box& box = m_boxes[i];
		if(box.colorIndex >= 0)
		{
			SDL_Rect rect;
			rect.x = m_position.x + ((i % m_width) * m_boxSize) - m_boxSize / 2.f;
			rect.y = 600 - (m_position.y + ((i / m_width) * m_boxSize)) - m_boxSize / 2.f;
			rect.w = m_boxSize;
			rect.h = m_boxSize;

			//printf("Box at %d %d\n", rect.x, rect.y);

			auto c = colorTable[box.colorIndex];
			SDL_SetRenderDrawColor(renderer, c.r, c.g, c.b, c.a);
			SDL_RenderFillRect(renderer, &rect);

			if (std::find(selection.begin(), selection.end(), BoxRef(i % m_width, i / m_width)) != selection.end())
			{
				SDL_SetRenderDrawColor(renderer, 255, 0, 255, 255);
				SDL_RenderDrawRect(renderer, &rect);
			}

			// Draw the highlight
			if (i == m_highlightedBox)
			{
				SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
				SDL_RenderDrawRect(renderer, &rect);
			}
		}
	}
}

void GameBoard::getAdjacentBoxes(int hindex, int vindex, int startColorIndex, std::vector<BoxRef>& results)
{
	// Out of bounds location, stop propagation
	if (hindex < 0 || hindex >= m_width || vindex < 0 || vindex >= m_height)
		return;
	
	// Insert itself and neighbours if applicable
	if(getBox(hindex, vindex).colorIndex == startColorIndex)
	{
		if (std::find(results.begin(), results.end(), BoxRef(hindex, vindex)) == results.end())
		{
			BoxRef boxRef;
			boxRef.x = hindex;
			boxRef.y = vindex;
			results.push_back(boxRef);

			getAdjacentBoxes(hindex + 1, vindex, startColorIndex, results);
			getAdjacentBoxes(hindex - 1, vindex, startColorIndex, results);
			getAdjacentBoxes(hindex, vindex + 1, startColorIndex, results);
			getAdjacentBoxes(hindex, vindex - 1, startColorIndex, results);
		}
	}
}
	
void GameBoard::handleClick(Vector2 position)
{
	printf("Mouse click at %f, %f\n", position.x, position.y);

	float localX = position.x - m_position.x;
	float localY = 600 - (position.y - m_position.y);
	
	int indexX = static_cast<int>(localX) / static_cast<int>(m_boxSize); 
	int indexY = static_cast<int>(localY) / static_cast<int>(m_boxSize);
	
	// Nothing happens when the mouse is out of bounds
	if (indexX < 0 || indexX >= m_width || indexY < 0 || indexY >= m_height)
		return;

	Box& clickBox = getBox(indexX, indexY);
	if(clickBox.colorIndex >= 0)
	{
		printf("CLICKED IN BOX %d\n", clickBox.colorIndex);

		std::vector<BoxRef> resultsArray;
		getAdjacentBoxes(indexX, indexY, clickBox.colorIndex, resultsArray);

		selection.clear();
		selection.insert(selection.end(), resultsArray.begin(), resultsArray.end());
		//return;

		/*if (resultsArray.size() == 1)
			return;*/

		// Destroy every box returned
		for (auto ref : resultsArray)
		{
			Box& box = getBox(ref.x, ref.y);
			box.colorIndex = -1;
		}

		for (auto ref : resultsArray)
		{
			Box& box = getBox(ref.x, ref.y);

			// Offset every box on top of the removed one down
			bool foundAnyBox = false;
			for (int i = ref.y + 1; i < m_height; ++i)
			{
				if(getBox(ref.x, i).colorIndex >= 0)
					foundAnyBox = true;
				
				getBox(ref.x, i - 1) = getBox(ref.x, i);
				getBox(ref.x, i).colorIndex = -1;
			}
			
			// Removed the last box on the column, a gap was formed, fix it.
			if(!foundAnyBox && ref.y == 0)
			{
				for (std::size_t j = ref.x; j >= 0; --j)
				{
					displaceColumn(j);
				}
			}
		}
	}
}

void GameBoard::displaceColumn(int hindex)
{
	for (std::size_t i = 0; i < m_height; ++i)
	{
		getBox(hindex + 1, i) = getBox(hindex, i);
		getBox(hindex, i).colorIndex = -1;
	}
}

void GameBoard::handleMouseMove(Vector2 position)
{
	float localX = position.x - m_position.x;
	float localY = 600 - (position.y - m_position.y);
	
	int indexX = static_cast<int>(localX) / static_cast<int>(m_boxSize);
	int indexY = static_cast<int>(localY) / static_cast<int>(m_boxSize);
	
	m_highlightedBox = indexY * m_width + indexX;
	
	// Check if mouse is out of bounds, or hits a empty slot
	if(m_highlightedBox < 0 || m_highlightedBox >= m_width * m_height)
	{
		m_highlightedBox = -1;
	}
	else if(getBox(indexX, indexY).colorIndex == -1)
	{
		m_highlightedBox = -1;
	}
}