#ifndef Miniclip_Button_h__
#define Miniclip_Button_h__

#include "Widget.h"
#include "Utilities.h"
#include <string>

class Button : public Widget
{
private:
	std::string m_text;
	
public:

	void setText(const std::string& text);
};

#endif // Miniclip_Button_h__