#ifndef Miniclip_Texture_h__
#define Miniclip_Texture_h__

#include "Utilities.h"
#include <string>

struct SDL_Surface;
struct SDL_Texture;
struct SDL_Renderer;

/**
	\class Texture

	Each live instance of a Texture is intended to contain
	its own image object, matching a file in the game directory.

	Its expected from SDL2 to have uploaded the image to the graphics card,
	if m_texture is valid.

*/
class Texture
{
private:
	
	SDL_Surface* m_bitmap;    //< The image pixel buffer. nullptr if not present
	SDL_Texture* m_texture;   //< The texture identifier, nullptr if not present

public:

	/// Uninitialized texture
	Texture();

	/// Ensure the texture is released
	~Texture();

	/// Returns the internal SDL texture identifier
	SDL_Texture* getTextureId();

	/// Loads a texture from a file using SDL_image
	bool loadFromFile(const std::string& assetPath, SDL_Renderer* renderer);
};

#endif // Miniclip_Texture_h__