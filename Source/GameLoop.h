#ifndef Miniclip_GameLoop_h__
#define Miniclip_GameLoop_h__

#include "GameBoard.h"
#include "AssetManager.h"

#include <SDL.h>
#include <SDL_image.h>

class GameLoop
{
private:
	bool          m_closing;
	float         m_accumulatedTime;
	GameBoard     m_board;
	SDL_Window*   m_window;
	SDL_Renderer* m_renderer;
	Uint32        m_previousTime;
	AssetManager  m_assetManager;
	
public:
	GameLoop();
	
	void init();

	int exec();
	
	void tick();
};

#endif // Miniclip_GameLoop_h__