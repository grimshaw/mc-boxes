#ifndef Miniclip_GameBoard_h__
#define Miniclip_GameBoard_h__

#include "Utilities.h"

#include <stdint.h>
#include <map>
#include <vector>
#include <assert.h>
#include <unordered_set>

#include <SDL.h>

extern std::map<uint16_t, Color> colorTable;


struct Box
{
	int     colorIndex;
	Vector2 position;
};

struct BoxRef
{
	int x;
	int y;

	BoxRef()
	{

	}

	BoxRef(int hindex, int vindex)
		: x(hindex)
		, y(vindex)
	{

	}

	bool operator==(const BoxRef& other)
	{
		return x == other.x && y == other.y;
	}
};

/// TODO, make const methods

/**
	The game's state is stored in the game board.
	
	When a game session begins, the board is initialized and becomes ready for playing. 
	All allocations are done at the initialization to prevent hiccups during execution,
	as memory allocation is a very expensive process.
	
	A std::vector is used to store the boxes, where each box slot can have a "null" state in case the given slot is empty.
	The boxes are laid out in memory column-major, so each column of boxes is contiguous in memory, as many tasks will 
	operate on columns rather than rows, which provides cache friendliness where possible.
	
	For convenience, the GameBoard API uses 2D indices into the slots, even though all slots are a single chunk of memory.
	
	Each box's logical position is inferred from its index in the std::vector, this is what is used for quick operations on the 
	table. Though, every box has a floating point position, to allow for animations. So, the box may be still be travelling to its final 
	position, but its logical position is already correct for subsequent operations.
*/
class GameBoard
{
private:
	std::vector<Box>      m_boxes;             //< Efficient storage for the boxes without any further allocations
	std::vector<Animator> m_animations;        //< Contains all game session related animations. The container grows as needed and stays efficient
	int                   m_width;             //< How many box slots there are horizontally
	int                   m_height;            //< How many box slots there are vertically
	int                   m_highlightedBox;    //< Current box hovered by the mouse or -1 if none
	float                 m_timeLeftForPush;   //< How much time (in seconds) remains until the column is pushed
	float                 m_boxSize;           //< The size, in pixels, of each box in the grid
	Vector2               m_position;          //< Position the game's table is in
	
public:
	
	/// Constructs the game board in an uninitialized state
	GameBoard();
	
	/// Initialize an empty board with a given size
	/// Any specific initial state must be then explicitly assigned
	void init(int w, int h);
	
	/// Generates boxes to randomly fill N columns (from the right)
	void generate(int columnCount);
	
	/// Advances the time forward, updating animations and timers
	void tick(float delta);
	
	/// Returns a modifiable reference to the specific box (or empty slot)
	Box& getBox(int hindex, int vindex);
	
	/// Builds a list of unique boxes with the same color, to be popped together from the grid
	/// This function uses recursion to propagate the "selection" through the entire grid
	/// Note: the containers are returned by value with no performance penalty due to compiler optimizations https://en.wikipedia.org/wiki/Return_value_optimization
	void getAdjacentBoxes(int hindex, int vindex, int startColorIndex, std::vector<BoxRef>& results);
	
	/// Checks if a given slot in the "table" is currently empty
	bool isSlotEmpty(int hindex, int vindex);
	
	/// Generates a new random column on the right and returns true if the game is lost as a consequence
	bool pushColumn();

	/// Displaces the column at index right (to fill gaps)
	void displaceColumn(int hindex);
	
	/// Sends all the boxes to be rendered
	void render(SDL_Renderer* renderer);
	
	/// Handles all mouse movements, to refresh the highlighted items, etc.
	void handleMouseMove(Vector2 position);
	
	/// Handles the click logic, in case it hits any usable box
	void handleClick(Vector2 position);	
};

#endif // Miniclip_GameBoard_h__