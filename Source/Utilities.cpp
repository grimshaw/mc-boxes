#include "Utilities.h"

std::mt19937 rng(time(0));

int RandomBoxColor()
{
	std::uniform_int_distribution<> dist(0, 11);
	return dist(rng);
}


Vector2::Vector2()
{	
}	
	
Vector2::Vector2(float xvalue, float yvalue)
: x(xvalue)
, y(yvalue)
{	
}

Vector2 Vector2::operator*(float scalar)
{
	return Vector2(x * scalar, y * scalar);
}

Vector2 Vector2::operator*(const Vector2& v)
{
	return Vector2(x * v.x, y * v.y);
}