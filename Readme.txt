Introduction
============
I made the prototype to resemble Ores as much as possible, without directly copying its elements
other than the game mechanics specified in the challenge rules. Given the tight time frame, I focused
the most on getting things to work correctly and less on animation and visuals, which are usually
more time consuming, in my opinion.

Since I had some delays on my schedule, including being unable to work at all the last weekend,
I could only spend a lot less time in the prototype than I 
was expecting. I apologize for it being less polished than I would like, but this is what I could
manage to complete until the deadline.


Built Binaries
==============
As requested, one prebuilt executable of the prototype can be found in the Binaries/ directory.
I don't have the possibility to build under OS X, so its a 32 bit Windows executable, to ensure
compatibility.


Repository
==========
During development of the prototype, the project was kept in a public git repository:
https://bitbucket.org/grimshaw/mc-boxes
Sources were ocasionally commited in bulk, so the commits aren't very accurate in terms of chronology.


Resolution
==========
For the time constraints, the assets and programming were done for a 1200x700 resolution,
picked for no special reason.


Visual assets
=============
The blocks were downloaded for free from www.opengameart.org and the remaining assets were
made by myself.


Audio assets
=============
The audio assets were downloaded from a free package found online.
The background music is from http://incompetech.com/wordpress/